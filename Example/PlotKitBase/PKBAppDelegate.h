//
//  PKBAppDelegate.h
//  PlotKitBase
//
//  Created by yunyyyun on 08/05/2019.
//  Copyright (c) 2019 yunyyyun. All rights reserved.
//

@import UIKit;

@interface PKBAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
