//
//  PKBViewController.m
//  PlotKitBase
//
//  Created by yunyyyun on 08/05/2019.
//  Copyright (c) 2019 yunyyyun. All rights reserved.
//

#import "PKBViewController.h"

#import "KlineHorizontalView.h"
#import "HXKlineCompDayData.h"

@interface PKBViewController ()

@property (strong, nonatomic) IBOutlet KlineHorizontalView *kLineView;
@property (assign, nonatomic) KlineIdxConfig config;
@property (strong, nonatomic) UIView *kLineSetBgView;
// @property (strong, nonatomic) KlineMenuSettingView *kLineSetView;
@property (assign, nonatomic) BOOL showSetMenue;
@property (strong, nonatomic) NSArray<HXKlineCompDayData *> *datas;

@end

@implementation PKBViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    _kLineView.backgroundColor = [UIColor whiteColor];
    self.showSetMenue = false;
    KlineIdxConfig config;
    
    config.priceIdxType = IDX_PriceIdx_MA;
    config.mainIdxType = IDX_VOLUMN;
    config.idxType = IDX_MACD;
    _config = config;
    [self setupKLineView: nil];
}

- (IBAction)setupKLineView:(id)sender {
    UIButton *btn = (UIButton *) sender;
    NSInteger tag = btn.tag;
    int count = (int)tag;
    if (count==0){
        count = 130;
    }
    //count = 10000;
    double lastClose = 1000.26;
    NSMutableArray<HXKlineCompDayData *> *datas = [[NSMutableArray alloc] init];
    for (int i=0; i<count; ++i){ // 模拟k线数据
        long openTime = 1553485500 + i * 900;
        long closeTime = 0;
        double openPrice = 100;
        double maxPrice = openPrice * (1 + (arc4random() % 7)/ 10.0);
        double minPrice = openPrice * (1 - (arc4random() % 7)/ 10.0);
        double closePrice = minPrice* (1 + (arc4random() % 11)/ 10.0);
        double money = 0;
        double total = 484518 + arc4random() % 284518;
        lastClose = closePrice;
        HXKlineCompDayData *d = [[HXKlineCompDayData alloc] init];
        d.m_openTime = openTime;
        d.m_closeTime = closeTime;
        d.m_lOpenPrice = openPrice;
        d.m_lMaxPrice = maxPrice;
        d.m_lMinPrice = minPrice;
        d.m_lClosePrice = closePrice;
        d.m_lMoney = money;
        d.m_lTotal = total;
        [datas addObject: d];
    }
    
    self.datas = datas;
    
    _kLineView.idxConfig = _config;
    [_kLineView updateStocks: datas];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
