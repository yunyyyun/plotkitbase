//
//  main.m
//  PlotKitBase
//
//  Created by yunyyyun on 08/05/2019.
//  Copyright (c) 2019 yunyyyun. All rights reserved.
//

@import UIKit;
#import "PKBAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PKBAppDelegate class]));
    }
}
