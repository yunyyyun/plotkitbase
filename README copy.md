# PlotKitBase

[![CI Status](https://img.shields.io/travis/yunyyyun/PlotKitBase.svg?style=flat)](https://travis-ci.org/yunyyyun/PlotKitBase)
[![Version](https://img.shields.io/cocoapods/v/PlotKitBase.svg?style=flat)](https://cocoapods.org/pods/PlotKitBase)
[![License](https://img.shields.io/cocoapods/l/PlotKitBase.svg?style=flat)](https://cocoapods.org/pods/PlotKitBase)
[![Platform](https://img.shields.io/cocoapods/p/PlotKitBase.svg?style=flat)](https://cocoapods.org/pods/PlotKitBase)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

PlotKitBase is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'PlotKitBase'
```

# How to use

#### Kline:

```objective-c
_kLineView.backgroundColor = [UIColor whiteColor];
self.showSetMenue = false;
KlineIdxConfig config;
config.priceIdxType = IDX_PriceIdx_MA;
config.mainIdxType = IDX_VOLUMN;
config.idxType = IDX_MACD;
NSMutableArray<HXKlineCompDayData *> *datas = [[NSMutableArray alloc] init];
... //just add data for datas
self.datas = datas;
```



#### TrendView:

```objective-c
self.priceTrendView.backgroundColor = [UIColor whiteColor];
self.priceTrendView.showVolume = false;
self.priceTrendView.showTime = true;
self.priceTrendView.showPrice = true;
self.priceTrendView.scrollable = true;
self.priceTrendView.type = PriceHistoryViewTypeFullHorizontalScreen;

NSMutableArray<HistoryPriceData *> *datas = [[NSMutableArray alloc] init];
... //just add data for datas
self.priceTrendView.datas = datas;
```



#### PieChart:

```objective-c
self.pieView.showLabel = false;
self.pieView.isHollow = true;
self.pieView.font = [UIFont systemFontOfSize:9];
... //set colors and pies
self.pieView.colors = colors;
self.pieView.pies = pies;
```



## Author

yunyyyun, mengyun@dae.org

## License

PlotKitBase is available under the MIT license. See the LICENSE file for more info.
